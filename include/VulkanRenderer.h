#pragma once
#define VK_USE_PLATFORM_WIN32_KHR
#include <vulkan/vulkan.hpp>
#include "glm/glm.hpp"
#include "Model.h"

/**
 * \class VulkanRenderer
 *
 * \brief Vulkan-based rendering system
 *
 * This class contains the core functionality for running basic Vulkan-based graphical applications. This class is not meant to 
 * be cross-platform at present, as it has Khronos extensions to specifically target Windows.
 *
 * \author Andrew Rossetter
 */
class VulkanRenderer
{
public:
  /**
   * \brief Constructor
   * \param application_name The name to appear in the title bar of the application.
   * \param application_version The version of the application (note: This is an integer, not a float or string)
   *
   * The constructor for this class creates the standard objects that will exist for the lifetime of the class itself.
   * This includes the graphics devices and command pools.
   */
	VulkanRenderer(const char* application_name, uint32_t application_version);

  /**
   * \brief Destructor
   *
   * Frees all objects created within the class. No additional calls to something like Free or Destroy are needed for this class.
   */
	~VulkanRenderer();

	/**
	 * \struct CameraDescriptor
   *
   * Our Uniform Buffer Object for tracking view matrices
	 */
	struct CameraDescriptor
	{
		glm::mat4 _view; ///< The Camera's View matrix
		glm::mat4 _projection; ///< The Scene's Projection Matrix
	};

  /**
   * \struct LightDescriptor
   *
   * Our Uniform Buffer Object for tracking light properties
   */
  struct LightDescriptor
  {
    glm::vec4 _light_position; ///< Position of the light (in world space)
    glm::vec4 _light_color; ///< Color of the light
  };

  /**
   * \brief Create a Win32 Surface
   * \param hinst The Win32 handle to the application instance
   * \param hwnd The Win32 handle to the window
   *
   * This function sets up the renderer to present into the window specified by hwnd. 
   * It should be called exactly one time, right after the constructor. Undefined (probably bad)
   * stuff will happen if you don't. 
   */
	void CreateWin32Surface(HINSTANCE hinst, HWND hwnd);

  /**
   * \brief Load GLSL Vertex and Fragment Shaders
   * \param vertex_shader The vertex shader to load.
   * \param fragment_shader The fragment shader to load.
   * \return The index with which to reference this shader combination.
   *
   * This function takes in two shaders meant to be used in the same rendering pass and returns the index
   * where they are stored. This index is referenced in the model structure to indicate the shader set that
   * the model is intended to be rendered with. Note that there is no validation that the shaders compiled
   * (compiling at runtime is not a normal use-case and is really only done for our demonstration purposes). 
   * This means that if the shaders fail to compile, the application WILL crash.
   */
	uint32_t LoadShaders(const std::vector<char> vertex_shader, const std::vector<char> fragment_shader);

  /**
   * \brief Clean out the list of shaders.
   * 
   * Used by our application to do live reloading of shaders while keeping our shader indexes intact.
   */
  void ClearShaders();

  /**
   * \brief Load Models into the renderer for drawing
   * \param models The array of models that are to be rendered
   *
   * In addition to storing off the model information, this function will also create a single vertex and index
   * buffer in which all models are stored, as well as load and store the textures those models use.
   */
	void LoadModels(const std::vector<Model> models);

  /**
   * \brief Clean out the current list of models.
   *
   * This function also clears out any textures and the vertex / index buffers. LoadModels() must be called after this if
   * the intention is to render anything.
   */
  void ClearModelList();

  /**
   * \brief Create the Swap Chain and render pipelines for rendering a scene.
   *
   * This function must be called after the surface is created. It is responsible for creating the swap chain images
   * as well as everything else that could potentially become invalidated during runtime (for example, by a window resize).
   * This must be called before RenderScene() or Present().
   */
	void PrepareRenderer();

  /**
   * \brief Create a command buffer to render a scene.
   *
   * Uses the command buffers to load the shaders, uniforms and vertex/index buffers and queue up drawing commands
   * into our command queue. This is where the drawing actually happens. This actually only needs to be called once,
   * as Present() will actually run the commands called here. 
   */
	void RenderScene();

  /**
   * \brief Submits a command buffer for rendering and presents an image to the screen.
   */
	void Present();

  /**
   * \brief Recreates the swap chain resources.
   *
   * Must be called anytime the window is resized. Will automatically be called by Present() if the swap chain reports that it is out of date.
   */
	void HandleResize();

  /**
   * \brief Update our camera information
   * \param ubo The updated camera view and projection matrices.
   *
   * Should be called if the camera moves in the scene. Since this is a UBO, there is no need to call RenderScene() after updating.
   */
	void UpdateCameraDescriptor(const CameraDescriptor &ubo);

  /**
   * \brief Update our light information
   * \param light The updated light position and color.
   *
   * Should be called if the light moves or changes color. Since this is a UBO, there is no need to call RenderScene() after updating.
   */
  void UpdateLightDescriptor(const LightDescriptor &light);

  /**
   * \brief Update the transformation matrix of the specified model.
   * \param model_id The ID of the model (corresponds to its index in the model array)
   * \param model_matrix The new model matrix for this model.
   *
   * Note: This function only needs to be called in the event the model's transform changes. If the model is static this
   * does not need to be called more than once (to set its original transform). Also, being a dynamic buffer, there is no
   * need to call RenderScene() after a model transform is updated.
   */
	void UpdateModelMatrix(uint32_t model_id, const glm::mat4 &model_matrix);
  
  /**
   * \brief Output device information to an output stream
   * \param out The output stream
   * \param renderer The class object.
   * \return an ostream for chaining.
   *
   * This function is designed to print out information related to the physical device chosen by the renderer to a stream.
   * Typical usage would be:
   * VulkanRenderer renderer("TestApp", 1);
   * std::cout << renderer << std::endl; // Prints information
   */
	friend std::ostream& operator<<(std::ostream& out, const VulkanRenderer& renderer);

private:

  /**
   * \struct SwapchainImageResources
   *
   * Container object for anything related to a swapchain (we have one of these per swapchain image).
   * In this application we are triple buffering, so we have an array of three of these in most cases.
   */
	struct SwapchainImageResources
	{
		vk::Image _image;
		vk::ImageView _image_view;
		vk::CommandBuffer _cmd_buffer;
		vk::Framebuffer _framebuffer;
	};

  /**
   * \struct TextureResources
   *
   * Container for everything related to a single texture in the system. We populate an array of these as models are loaded.
   */
	struct TextureResources
	{
		vk::DescriptorImageInfo _image_info;
		vk::Image _image;
		vk::ImageView _image_view;
		vk::DeviceMemory _image_memory;
	};

  /**
   * \struct ShaderResources
   *
   * Container that keeps together related vertex and fragment shader modules.
   */
  struct ShaderResources
  {
    vk::ShaderModule _vertex_shader;
    vk::ShaderModule _fragment_shader;
  };

  /**
   * \struct DescriptorResources
   *
   * Container that keeps together buffers and memory related to our descriptors.
   */
  struct DescriptorResources
  {
    vk::Buffer _buffer;
    vk::DeviceMemory _buffer_memory;
    vk::DescriptorBufferInfo _buffer_info;
  };

	/**
   * \struct ModelInternal
   *
   * Data required for rendering models in our scene, including their textures and offsets into the vertex/index buffers.
   */
	struct ModelInternal
	{
		TextureResources _diffuse_map;
		uint32_t _vertex_offset;
		uint32_t _vertex_size;
		uint32_t _index_offset;
		uint32_t _index_size;

    uint32_t _shader_index;
	};


  /**
   * \struct UBODataDynamic
   *
   * The description of our dynamic buffer, which for now is nothing more than a transformation matrix for models. 
   */
	struct UBODataDynamic
	{
		glm::mat4* _buffer;
	};

  /**
   * \brief Create our Vulkan Instance
   * \param application_name The name of the application
   * \param application_version The version number
   * \return A Vulkan instance object.
   *
   * Called by the constructor to allocate our Vulkan application instance. 
   */
	vk::Instance CreateVulkanInstance(const char* application_name, uint32_t application_version) const;

  /**
   * \brief Given our physical devices, create a logical device
   * \return The Logical Device created from the chosen physical device.
   *
   * This function queries the physical devices on the machine, chooses one, and creates a logical device.
   * Additional side effects include choosing a proper queue index for Rendering.
   */
	vk::Device CreateLogicalDevice();

  /**
   * \brief Create the Descriptor Pool for the Application
   * \return a Descriptor Pool object
   *
   * Note that we hardcode our descriptors in this application, so any changes (adding UBOs, push constants or dynamic buffers)
   * will need to be reflected here as well. This function is called in the constructor.
   */
	vk::DescriptorPool CreateDescriptorPool() const;

  /**
   * \brief Create the buffers for the UBOs (Camera and Light)
   */
	void CreateUniformBuffer();

  /**
   * \brief Create the descriptor set layout used by the renderer.
   * \return The Descriptor set layout object.
   */
	vk::DescriptorSetLayout CreateDescriptorSetLayout() const;

  /**
   * \brief Create a descriptor set
   * \param descriptor_pool The memory pool from which to create this set.
   * \param descriptor_set_layout Description of how the descriptor set is laid out
   * \return A new descriptor set
   */
	vk::DescriptorSet CreateDescriptorSet(vk::DescriptorPool descriptor_pool, vk::DescriptorSetLayout descriptor_set_layout) const;

  /**
   * \brief Create a pipeline layout
   * \param descriptor_set_layout Description of how the descriptor set is laid out
   * \return A new pipeline layout
   */
	vk::PipelineLayout CreatePipelineLayout(vk::DescriptorSetLayout descriptor_set_layout) const;

  /**
   * \brief Create the semaphores used in the Present function
   */
	void CreateSemaphores();

  /**
   * \brief Load a texture onto the GPU
   * \param texture The image data for the texture
   * \param sampler The sampler to be used to reference this texture in the fragment shader
   * \return The renderer-internal representation of this texture
   *
   * Note: Once the texture is loaded in memory, it is safe to free the pixel data.
   */
	TextureResources LoadTexture(const Texture &texture, vk::Sampler sampler) const;

  /**
   * \brief Allocate the dynamic buffer object that stores our model transforms
   */
	void AllocateDynamicBuffer();

  /**
   * \brief Create a buffer for our geometry (vertex or index)
   * \param data The data to be loaded into the buffer
   * \param buffer_type description of the data in the buffer
   * \return The buffer and handle to the device memory where the data is stored.
   *
   * Note: Current types used for T are Vertex (for vertex buffers) and uint32_t (for index buffers) 
   */
	template<typename T>
	std::pair<vk::Buffer, vk::DeviceMemory> CreateGeometryBuffer(const std::vector<T> &data, vk::BufferUsageFlagBits buffer_type) const;
	
  /**
   * \brief Creates the swapchain used for rendering
   */
	void CreateSwapchain();

  /**
   * \brief Create the render pass
   * \return The RenderPass Vulkan object
   */
	vk::RenderPass CreateRenderPass() const;

  /**
   * \brief Create the depth buffer and device memory handle
   */
	void CreateDepthResources();

  /**
   * \brief Create the renderer's framebuffers
   */
	void CreateFramebuffers();

  /**
   * \brief Create the graphics pipelines
   *
   * Note: A pipeline is created for each shader.
   */
	void CreateGraphicsPipelines();

	std::vector<SwapchainImageResources> _swapchain_resources;
	std::vector<ModelInternal> _models;

	std::pair<vk::Buffer, vk::DeviceMemory> _vertex_buffer;
	std::pair<vk::Buffer, vk::DeviceMemory> _index_buffer;

	vk::Instance _instance;
	vk::PhysicalDevice _gpu;
	vk::PhysicalDeviceProperties _gpu_props;
	vk::Device _device;
	
	vk::Queue _draw_queue;
	vk::Queue _present_queue;
	
	vk::CommandPool _cmd_pool;
	
	vk::DescriptorPool _descriptor_pool;
	vk::DescriptorSetLayout _descriptor_set_layout;
	vk::DescriptorSet _descriptor_set;

	vk::Sampler _diffuse_sampler;

  DescriptorResources _view_descriptor;
  DescriptorResources _light_descriptor;

	UBODataDynamic _model_matrix_list;
	uint32_t _dynamic_buffer_alignment;
  DescriptorResources _dynamic_descriptor;

	vk::Semaphore _image_available;
	vk::Semaphore _render_finished;

	vk::SurfaceKHR _surface;
	vk::SurfaceFormatKHR _surface_format;
	vk::Extent2D _surface_extents;

	vk::SwapchainKHR _swapchain;

	vk::RenderPass _render_pass;

	std::vector<vk::Pipeline> _pipelines;

	vk::PipelineLayout _graphics_pipeline_layout;

  std::vector<ShaderResources> _shaders;

	vk::Image _depth_image;
	vk::DeviceMemory _depth_image_mem;
	vk::ImageView _depth_image_view;

	uint32_t _queue_family_index;
};


