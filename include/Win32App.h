#pragma once
#include <Windows.h>
#include <functional>

// Typedefs for callback functions
typedef std::function<void(uint32_t)> KeyUpCb;
typedef std::function<void(uint16_t, uint16_t)> MouseMoveCb;
typedef std::function<void(uint16_t, uint16_t)> ResizeCb;
typedef std::function<void(uint16_t, uint16_t)> LeftBtnDownCb;
typedef std::function<void()> LeftBtnUpCb;
typedef std::function<void(int32_t, int32_t)> MouseWheelCb;

/**
 * \class Win32App
 * \brief Handles window creation and event processing for a Win32 Application
 *
 * This class is responsible for creating a win32 window as well as holding onto the message pump for that window.
 * Applications that use this class set up callbacks for specific events that they would like to process.
 *
 * \author Andrew Rossetter
 */
class Win32App
{
public:
  /**
   * \brief Constructor
   */
	Win32App();

  /**
   * \brief Destructor
   */
	~Win32App();

  /**
   * \brief Create the Win32 Window for the application
   * \param class_name The class name provided to the Window creation struct
   * \param window_title The title to be shown on the window frame
   * \param width The desired width of the window
   * \param height The desired height of the window
   */
	void CreateAppWindow(const char* class_name, const char* window_title, int width, int height);

  /**
   * \brief Process any events in the message pump
   * \return false if the application processed a WM_QUIT message
   */
	bool ProcessUpdate();

  /**
   * \brief Get the handle to the window
   * \return the hwnd
   */
	inline HWND GetHWND() { return _hwnd; }

  /**
   * \brief Get the handle to the application instance
   * \return the hinstance
   */
	inline HINSTANCE GetInstance() { return _hinst; }

  /**
   * \brief Set the callback function used to handle key up messages
   * \param keyup_cb The callback
   *
   * The Key Up callback has the following prototype:
   * void keyup(uint32_t key);
   * Params: key The key that was pressed. Could be a VK code or ascii character depending on the press
   */
	inline void SetOnKeyupCb(KeyUpCb keyup_cb) { _keyup_cb = keyup_cb; }

  /**
   * \brief Set the callback function used to handle mouse move messages
   * \param mousemove_cb The callback
   *
   * The mouse move callback has the following prototype: 
   * void mousemove(uint16_t x, uint16_t y);
   * Params:
   * x The x coordinate of the mouse location relative to the window (top left is 0,0)
   * y The y coordinate of the mouse location relative to the window
   */
	inline void SetMouseMoveCb(MouseMoveCb mousemove_cb) { _mousemove_cb = mousemove_cb; }

  /**
   * \brief Set the callback function used to handle resize messages
   * \param resize_cb The callback
   *
   * The resize callback has the following prototype:
   * void resize(uint16_t width, uint16_t height);
   * Params:
   * width The new width of the window
   * height The new height of the window
   */
	inline void SetResizeCb(ResizeCb resize_cb) { _resize_cb = resize_cb; }


  /**
   * \brief Set the callback function used to handle left mouse button down messages
   * \param left_button_cb The callback
   *
   * The left mouse button down callback has the following prototype:
   * void left_mouse_down(uint16_t x, uint16_t y);
   * Params:
   * x The x coordinate of the mouse location relative to the window (top left is 0,0)
   * y The y coordinate of the mouse location relative to the window
   */
	inline void SetLeftButtonDownCb(LeftBtnDownCb left_button_cb) { _left_button_down_cb = left_button_cb; }

  /**
   * \brief Set the callback function used to handle left mouse button up messages
   * \param left_button_up_cb The callback
   *
   * The left mouse button up callback has the following prototype:
   * void left_mouse_up();
   */
	inline void SetLeftButtonUpCb(LeftBtnUpCb left_button_up_cb) { _left_button_up_cb = left_button_up_cb; }

  /**
   * \brief Set the callback function used to handle mouse wheel messages
   * \param mouse_wheel_cb The callback
   *
   * The mouse wheel callback has the following prototype:
   * void mouse_wheel(int32_t wheel_val, int32_t wheel_delta);
   * Params:
   * wheel_val The distance the wheel has been rolled
   * wheel_delta The value for rolling the wheel one detent. Set by Win32 as 120
   */
	inline void SetMouseWheelCb(MouseWheelCb mouse_wheel_cb) { _mouse_wheel_cb = mouse_wheel_cb; }

private:
	static LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
	LRESULT WndProc_Internal(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
	HINSTANCE _hinst;
	HWND _hwnd;

	KeyUpCb _keyup_cb;
	MouseMoveCb _mousemove_cb;
	ResizeCb _resize_cb;
	LeftBtnDownCb _left_button_down_cb;
	LeftBtnUpCb _left_button_up_cb;
	MouseWheelCb _mouse_wheel_cb;
};