#pragma once
#include <vector>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/hash.hpp"
#include "glm/glm.hpp"


/**
 * \struct Vertex
 *
 * Public accessible Vertex declaration for use in demos
 */
struct Vertex
{
  glm::vec3 pos; ///< The Vertex Position
  glm::vec3 normal; ///< The vertex Normal
  glm::vec2 tex_coord; ///< Vertex Texture Coordinate

  /**
   * \brief comparison operator
   * \param other The Vertex object to compare against
   * \return true if the other Vertex is equal to this one
   */
  bool operator ==(const Vertex& other) const
  {
    return pos == other.pos && tex_coord == other.tex_coord && normal == other.normal;
  }
};

/**
 * \brief inline replacement for boost::hash_combine
 *
 * Used in the () operator for Vertex to construct a hash.
 */
template <typename T>
inline void hash_combine(std::size_t & s, const T & v)
{
  std::hash<T> h;
  s ^= h(v) + 0x9e3779b9 + (s << 6) + (s >> 2);
}

namespace std
{
  template <> struct hash<Vertex>
  {
    /**
     * \brief Hash function for our vertex type. Used for the unordered map
     */
    std::size_t operator()(Vertex const& s) const
    {
      std::size_t res = 0;

      hash_combine(res, s.pos);
      hash_combine(res, s.normal);
      hash_combine(res, s.tex_coord);

      return res;
    }
  };
}

/**
 * \struct Texture
 * 
 * The Texture struct, containing data needed by the renderer to load an image
 */
struct Texture
{
  uint8_t *pixels; ///< Pixel data for the texture. Is an array of size width*height*channels
  int32_t width; ///< The Width of the texture
  int32_t height; ///< Height of the texture
  int32_t channels; ///< Number of channels in the texture (Will usually be 4 for RGBA)
};

/** 
 * \struct Model
 * 
 * The Model struct, holder of vertex and index lists as well as texture and material info.
 */
struct Model
{
  std::vector<Vertex> _vertices; ///< List of vertices
  std::vector<uint32_t> _indices; ///< List of indices into the vertex array

  Texture _diffuse_map; ///< The model's diffuse texture

  glm::mat4 _model_matrix; ///< Position for this object

  uint32_t _shader_index; ///< Index to the shader used to render this object
};