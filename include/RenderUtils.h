#pragma once
#include <vulkan\vulkan.hpp>
/**
 * RenderUtils.h: Helper functions for common operations done in the renderer.
 */

/**
 * \brief The Render Utilities Namespace. Holds a collection of helper functions for Vulkan applications
 */
namespace RenderUtils
{
	/**
	 * \brief Create image
	 */
	std::pair<vk::Image, vk::DeviceMemory> CreateImage(vk::Device device, vk::PhysicalDevice gpu, uint32_t width, uint32_t height, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties);
	
	/**
	 * \brief Create image view
	 */
	vk::ImageView CreateImageView(vk::Device device, vk::Image image, vk::Format format, vk::ImageAspectFlags aspect_flags);

	/**
	 * \brief Create a texture sampler
	 */
	vk::Sampler CreateTextureSampler(vk::Device device);

	/**
	 * \brief Create buffer
	 */
	std::pair<vk::Buffer, vk::DeviceMemory> CreateBuffer(vk::Device device, vk::PhysicalDevice gpu, vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties);

	/**
	 * \brief Copy one buffer to another
	 */
	void CopyBuffer(vk::Device device, vk::CommandPool command_pool, vk::Queue queue, vk::Buffer src_buffer, vk::Buffer dst_buffer, vk::DeviceSize size);

	/**
	 * \brief Start recording of our command buffer for single command groups.
	 */
	vk::CommandBuffer BeginSingleTimeCommands(vk::Device device, vk::CommandPool command_pool);

  /**
   * \brief Stop recording of our command buffer for single command groups.
   */
	void EndSingleTimeCommands(vk::Device device, vk::CommandPool command_pool, vk::CommandBuffer cmd_buffer, vk::Queue queue);

	/**
	 * \brief Transition an image from one state to another
	 */
	void TransitionImageLayout(vk::Device device, vk::CommandPool cmd_pool, vk::Queue queue, vk::Image image, vk::Format format, vk::ImageLayout old_layout, vk::ImageLayout new_layout);

	/**
	 * \brief Copy the contents of a buffer into an image
	 */
	void CopyBufferToImage(vk::Device device, vk::CommandPool command_pool, vk::Queue queue, vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height);

	/**
	 * \brief Get the correct device memroy for a given buffer
   * \param gpu The physical device to query
	 * \param type_filter The filter returned from the buffer memory requirements struct
	 * \param properties The desired properties for the memory block
	 * \return The memory index to use.
	 */
	uint32_t FindMemoryType(vk::PhysicalDevice gpu, uint32_t type_filter, vk::MemoryPropertyFlags properties);
};