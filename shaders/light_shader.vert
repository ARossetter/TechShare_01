#version 450 core

// Uniforms
layout(binding = 0) uniform CAMERA 
{
	mat4 view;
	mat4 proj;
} cam;

layout(binding = 2) uniform PER_OBJECT
{
	mat4 model;
} mdl;

layout(binding = 3) uniform LIGHT_PROPS
{
	vec4 light_pos;
	vec4 light_color;
} light;

// Vertex Attributes 
layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec2 a_tex_coord;

// Outputs
layout(location = 0) out vec2 tex_coord_out;
layout(location = 1) out vec3 position_out;
layout(location = 2) out vec3 normal_out;
layout(location = 3) out vec3 light_pos_out;
layout(location = 4) out vec4 light_color_out;

out gl_PerVertex { vec4 gl_Position; };

// Shader Entry Point
void main() 
{
	// We use our ModelView matrix to project our vert and normal to eye space
    mat4 MVMatrix = cam.view * mdl.model;

	// Transform vertex and light positions to eye space
	// We use these in the fragment shader to calculate lighting
	position_out = vec3(MVMatrix * vec4(a_position, 1.0));
	normal_out = vec3(MVMatrix * vec4(a_normal, 0.0));
	light_pos_out = vec3(cam.view * light.light_pos);

	tex_coord_out = a_tex_coord;
	light_color_out = light.light_color;
    gl_Position = cam.proj * cam.view * mdl.model * vec4(a_position, 1.0);
}