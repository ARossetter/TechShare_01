@ECHO OFF
glslc.exe bb_shader.vert -o ./SPIR/bb_vert.spv
glslc.exe bb_shader.frag -o ./SPIR/bb_frag.spv
glslc.exe light_shader.vert -o ./SPIR/light_vert.spv
glslc.exe light_shader.frag -o ./SPIR/light_frag.spv
