#version 450 core

// Uniforms
layout(binding = 1) uniform sampler2D diffuse_map;

// Inputs
layout(location = 0) in vec2 tex_coord;
layout(location = 1) in vec3 frag_position;
layout(location = 2) in vec3 frag_normal;
layout(location = 3) in vec3 light_position;
layout(location = 4) in vec4 light_color;

// Outputs
layout(location = 0) out vec4 fragment_color_out;


/**************************************************************/
/***				Lighting Functions						***/
/**************************************************************/
float CalculateLightAttenuation(vec3 light_position, vec3 frag_position, float attenuation_factor)
{
	float light_distance = length(light_position - frag_position);
	return 1.0 / (1.0 + attenuation_factor * pow(light_distance, 2));
}

vec3 CalculateAmbient(vec3 fragment_color, vec3 ambient)
{
	return ambient * fragment_color;
}

vec3 CalculateDiffuse(vec3 fragment_color, vec3 light_position, vec3 frag_position, vec3 normal, vec3 light_diffuse)
{
	vec3 light_dir = normalize(light_position - frag_position);
	float cosine_theta = max(dot(light_dir, normal), 0.0);

	return cosine_theta * light_diffuse * fragment_color;
}

vec3 CalculateSpecular(vec3 fragment_color, vec3 light_position, vec3 frag_position, vec3 normal, vec3 light_specular, float specular_coefficient)
{
	vec3 surface_to_camera = normalize(-frag_position); // Since we're in eye space, camera is at (0,0,0)
	vec3 light_dir = normalize(frag_position - light_position);
	vec3 reflection = reflect(light_dir, normal);
	float cosine_theta = max(0.0, dot(surface_to_camera, reflection));
	float specular_value = pow(cosine_theta, specular_coefficient);

	return specular_value * light_specular;
}


/**************************************************************/
/***				Shader Entry Point						***/
/**************************************************************/
void main() 
{
	// "Globals" for our lighting calculations. Would normally be provided as uniforms to the shader.
	vec3 ambient = vec3(0.02, 0.02, 0.02);
	float attenuation_factor = 0.1;
	float specular_coefficient = 35.0;

	// Sample the texture to get our base pixel color.
	vec4 texture_color = texture(diffuse_map, tex_coord);

	// Variables that hold our lighting color values
	float attenuation = CalculateLightAttenuation(light_position, frag_position, attenuation_factor);
	vec3 ambient_color = CalculateAmbient(texture_color.rgb, ambient);
	vec3 diffuse_color = CalculateDiffuse(texture_color.rgb, light_position, frag_position, frag_normal, light_color.rgb);
	vec3 specular_color = CalculateSpecular(texture_color.rgb, light_position, frag_position, frag_normal, light_color.rgb, specular_coefficient);	

	// The final fragment color is the sum of our lighting calcuations.
	vec3 fragment_color = ambient_color + attenuation * (diffuse_color + specular_color);

	// We don't mess with alpha during lighting calculations, so just use the texture's alpha for that component.
    fragment_color_out = vec4(fragment_color, texture_color.a);
}