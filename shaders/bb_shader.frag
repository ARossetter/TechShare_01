#version 450 core

// Uniforms
layout(binding = 1) uniform sampler2D diffuse_map;

// Inputs
layout(location = 0) in vec2 tex_coord;
layout(location = 1) in vec4 light_color;

// Outputs
layout(location = 0) out vec4 out_color;

// Shader Entry Point
void main() 
{
	vec4 tex_color = texture(diffuse_map, tex_coord);
    out_color = vec4(light_color.rgb * tex_color.rgb, tex_color.a);
}