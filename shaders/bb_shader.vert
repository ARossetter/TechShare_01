#version 450 core

// Uniforms
layout(binding = 0) uniform CAMERA 
{
	mat4 view;
	mat4 proj;
} cam;

layout(binding = 2) uniform PER_OBJECT
{
	mat4 model;
} mdl;

layout(binding = 3) uniform LIGHT_PROPS
{
	vec4 light_pos;
	vec4 light_color;
} light;

// Vertex Attributes 
layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec2 a_tex_coord;

// Outputs
layout(location = 0) out vec2 tex_coord;
layout(location = 1) out vec4 light_color;

out gl_PerVertex { vec4 gl_Position; };

// Shader Entry Point
void main() 
{
	mat4 model_view_matrix = cam.view * mdl.model;

	model_view_matrix[0][0] = 1.0;
	model_view_matrix[0][1] = 0.0;
	model_view_matrix[0][2] = 0.0;

	model_view_matrix[1][0] = 0.0;
	model_view_matrix[1][1] = 1.0;
	model_view_matrix[1][2] = 0.0;

	model_view_matrix[2][0] = 0.0;
	model_view_matrix[2][1] = 0.0;
	model_view_matrix[2][2] = 1.0;


    gl_Position = cam.proj * model_view_matrix * vec4(a_position, 1.0);
    tex_coord = a_tex_coord;
    light_color = light.light_color;
}