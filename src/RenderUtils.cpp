#include "RenderUtils.h"

namespace RenderUtils
{
	std::pair<vk::Image, vk::DeviceMemory> CreateImage(vk::Device device, vk::PhysicalDevice gpu, uint32_t width, uint32_t height, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties)
	{
		// Create the image...
		const auto image_ci = vk::ImageCreateInfo()
			.setImageType(vk::ImageType::e2D)
			.setExtent(vk::Extent3D(width, height, 1))
			.setMipLevels(1)
			.setArrayLayers(1)
			.setFormat(format)
			.setTiling(tiling)
			.setInitialLayout(vk::ImageLayout::eUndefined)
			.setUsage(usage)
			.setSharingMode(vk::SharingMode::eExclusive)
			.setSamples(vk::SampleCountFlagBits::e1);

		auto image = device.createImage(image_ci);
		
		// Now create the memory to store the image...
		auto mem_reqs = device.getImageMemoryRequirements(image);
		const auto memory_ai = vk::MemoryAllocateInfo()
			.setAllocationSize(mem_reqs.size)
			.setMemoryTypeIndex(FindMemoryType(gpu, mem_reqs.memoryTypeBits, properties));
		auto image_memory = device.allocateMemory(memory_ai);

		// Bind the two...
		device.bindImageMemory(image, image_memory, 0);

		// And Return.
		return std::pair<vk::Image, vk::DeviceMemory>(image, image_memory);
	}

	vk::ImageView CreateImageView(vk::Device device, vk::Image image, vk::Format format, vk::ImageAspectFlags aspect_flags)
	{
		const auto view_ci = vk::ImageViewCreateInfo()
			.setImage(image)
			.setViewType(vk::ImageViewType::e2D)
			.setFormat(format)
			.setComponents(vk::ComponentSwizzle::eIdentity)
			.setSubresourceRange(vk::ImageSubresourceRange(aspect_flags, 0, 1, 0, 1));

		return device.createImageView(view_ci);
	}

	vk::Sampler CreateTextureSampler(vk::Device device)
	{
		const auto sampler_ci = vk::SamplerCreateInfo()
			.setMagFilter(vk::Filter::eLinear)
			.setMinFilter(vk::Filter::eLinear)
			.setAddressModeU(vk::SamplerAddressMode::eRepeat)
			.setAddressModeV(vk::SamplerAddressMode::eRepeat)
			.setAddressModeW(vk::SamplerAddressMode::eRepeat)
			.setAnisotropyEnable(VK_TRUE)
			.setMaxAnisotropy(16)
			.setBorderColor(vk::BorderColor::eIntOpaqueBlack)
			.setUnnormalizedCoordinates(VK_FALSE)
			.setCompareEnable(VK_FALSE)
			.setCompareOp(vk::CompareOp::eAlways)
			.setMipmapMode(vk::SamplerMipmapMode::eLinear)
			.setMipLodBias(0.0f)
			.setMinLod(0.0f)
			.setMaxLod(0.0f);

		return device.createSampler(sampler_ci);
	}

	std::pair<vk::Buffer, vk::DeviceMemory> CreateBuffer(vk::Device device, vk::PhysicalDevice gpu, vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties)
	{
		const auto buffer_ci = vk::BufferCreateInfo()
			.setSize(size)
			.setUsage(usage)
			.setSharingMode(vk::SharingMode::eExclusive);

		auto buffer = device.createBuffer(buffer_ci);

		// Now allocate memory for the new buffer.
		const auto mem_reqs = device.getBufferMemoryRequirements(buffer);

		auto mem_index = FindMemoryType(gpu, mem_reqs.memoryTypeBits, properties);

		const auto memory_ai = vk::MemoryAllocateInfo()
			.setAllocationSize(mem_reqs.size)
			.setMemoryTypeIndex(mem_index);

		auto buffer_memory = device.allocateMemory(memory_ai);

		// Before returning the buffer and its memory, bind the two.
		device.bindBufferMemory(buffer, buffer_memory, 0);

		return std::pair<vk::Buffer, vk::DeviceMemory>(buffer, buffer_memory);
	}

	void CopyBuffer(vk::Device device, vk::CommandPool command_pool, vk::Queue queue, vk::Buffer src_buffer, vk::Buffer dst_buffer, vk::DeviceSize size)
	{
		auto command_buffer = BeginSingleTimeCommands(device, command_pool);

		const auto copy_region = vk::BufferCopy()
			.setSize(size);

		command_buffer.copyBuffer(src_buffer, dst_buffer, copy_region);

		EndSingleTimeCommands(device, command_pool, command_buffer, queue);
	}

	vk::CommandBuffer BeginSingleTimeCommands(vk::Device device, vk::CommandPool command_pool)
	{
		const auto buffer_ai = vk::CommandBufferAllocateInfo()
			.setLevel(vk::CommandBufferLevel::ePrimary)
			.setCommandPool(command_pool)
			.setCommandBufferCount(1);

		auto command_buffer = device.allocateCommandBuffers(buffer_ai);

		const auto buffer_begin_info = vk::CommandBufferBeginInfo()
			.setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

		command_buffer[0].begin(buffer_begin_info);

		return command_buffer[0];
	}

	void EndSingleTimeCommands(vk::Device device, vk::CommandPool command_pool, vk::CommandBuffer cmd_buffer, vk::Queue queue)
	{
		cmd_buffer.end();

		const auto submit_info = vk::SubmitInfo()
			.setCommandBufferCount(1)
			.setPCommandBuffers(&cmd_buffer);

		queue.submit(1, &submit_info, nullptr);
		queue.waitIdle();

		device.freeCommandBuffers(command_pool, cmd_buffer);
	}

	void TransitionImageLayout(vk::Device device, vk::CommandPool cmd_pool, vk::Queue queue, vk::Image image, vk::Format format, vk::ImageLayout old_layout, vk::ImageLayout new_layout)
	{
		auto cmd_buffer = BeginSingleTimeCommands(device, cmd_pool);

		vk::PipelineStageFlags source_stage = vk::PipelineStageFlagBits(0);
		vk::PipelineStageFlags destination_stage = vk::PipelineStageFlagBits(0);
		vk::AccessFlags source_flags = vk::AccessFlagBits(0);
		vk::AccessFlags destination_flags = vk::AccessFlagBits(0);

		if (vk::ImageLayout::eUndefined == old_layout && vk::ImageLayout::eTransferDstOptimal == new_layout)
		{
			source_stage = vk::PipelineStageFlagBits::eTopOfPipe;
			destination_stage = vk::PipelineStageFlagBits::eTransfer;

			destination_flags = vk::AccessFlagBits::eTransferWrite;
		}
		else if (vk::ImageLayout::eTransferDstOptimal == old_layout && vk::ImageLayout::eShaderReadOnlyOptimal == new_layout)
		{
			source_stage = vk::PipelineStageFlagBits::eTransfer;
			destination_stage = vk::PipelineStageFlagBits::eFragmentShader;

			source_flags = vk::AccessFlagBits::eTransferWrite;
			destination_flags = vk::AccessFlagBits::eShaderRead;
		}
		else if (vk::ImageLayout::eUndefined == old_layout && vk::ImageLayout::eDepthStencilAttachmentOptimal == new_layout)
		{
			source_stage = vk::PipelineStageFlagBits::eTopOfPipe;
			destination_stage = vk::PipelineStageFlagBits::eEarlyFragmentTests;

			destination_flags = vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite;
		}
		else
		{
			throw std::runtime_error("ERROR: Unsupported image layout transition requested.");
		}

		const auto barrier = vk::ImageMemoryBarrier()
			.setOldLayout(old_layout)
			.setNewLayout(new_layout)
			.setSrcQueueFamilyIndex(~0U)
			.setDstQueueFamilyIndex(~0U)
			.setImage(image)
			.setSubresourceRange(vk::ImageSubresourceRange(vk::ImageLayout::eDepthStencilAttachmentOptimal == new_layout ? vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil : vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1))
			.setSrcAccessMask(source_flags)
			.setDstAccessMask(destination_flags);

		cmd_buffer.pipelineBarrier(source_stage, destination_stage, vk::DependencyFlagBits(0), 0, nullptr, 0, nullptr, 1, &barrier);

		EndSingleTimeCommands(device, cmd_pool, cmd_buffer, queue);
	}

	void CopyBufferToImage(vk::Device device, vk::CommandPool command_pool, vk::Queue queue, vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height)
	{
		auto command_buffer = BeginSingleTimeCommands(device, command_pool);

		const auto region = vk::BufferImageCopy()
			.setBufferOffset(0)
			.setBufferRowLength(0)
			.setBufferImageHeight(0)
			.setImageSubresource(vk::ImageSubresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1))
			.setImageOffset(vk::Offset3D(0, 0, 0))
			.setImageExtent(vk::Extent3D(width, height, 1));

		command_buffer.copyBufferToImage(buffer, image, vk::ImageLayout::eTransferDstOptimal, region);

		EndSingleTimeCommands(device, command_pool, command_buffer, queue);
	}

	uint32_t FindMemoryType(vk::PhysicalDevice gpu, uint32_t type_filter, vk::MemoryPropertyFlags properties)
	{
		auto index = UINT_MAX;

		auto memory_props = gpu.getMemoryProperties();

		for (uint32_t i = 0; i < memory_props.memoryTypeCount; ++i)
		{
			if (type_filter & (1 << i) && ((memory_props.memoryTypes[i].propertyFlags & properties) == properties))
			{
				index = i;
				break;
			}
		}

		if (UINT_MAX == index)
		{
			throw std::runtime_error("ERROR: Cannot find suitable memory type for buffer.");
		}

		return index;
	}
};