#include "VulkanRenderer.h"
#include "RenderUtils.h"
#include <iostream>

// For easy editing and recollection, these are the layers and extensions that we are using in the system.
namespace
{
	const char* instance_layer_names[] = { "VK_LAYER_KHRONOS_validation" };
	const uint32_t instance_layer_count = sizeof(instance_layer_names) / sizeof(instance_layer_names[0]);

	const char* instance_extension_names[] = { VK_KHR_SURFACE_EXTENSION_NAME, "VK_KHR_win32_surface" };
	const uint32_t instance_extension_count = sizeof(instance_extension_names) / sizeof(instance_extension_names[0]);

	const char* device_extension_names[] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
	const uint32_t device_extension_count = sizeof(device_extension_names) / sizeof(device_extension_names[0]);

	const uint32_t NUM_IMAGES = 3;
	const std::array<float, 4> CLR_CORNFLOWER_BLUE = { 0.392f, 0.584f, 0.929f, 1.0f };
	const std::array<float, 4> CLR_BLACK = { 0.0f, 0.0f, 0.0f, 1.0f };
}

VulkanRenderer::VulkanRenderer(const char * application_name, uint32_t application_version)
{
	// Create our Vulkan Instance
	_instance = CreateVulkanInstance(application_name, application_version);

	// Grab a physical device
	auto physical_devices = _instance.enumeratePhysicalDevices();
	if (physical_devices.size() > 0)
	{
		// Just take the first one for this example.
		// TODO Actually query the devices to find the best one.
		_gpu = physical_devices[0];
		_gpu_props = _gpu.getProperties();
	}
	else
		throw std::runtime_error("ERROR: No Vulkan compatible device found");

	// Create a logical device for our chosen physical device.
	_device = CreateLogicalDevice();

	// And create two queues: One for drawing and one for presentation.
	// Note: We are using the same family index, so this will be the same queue. TODO.
	_draw_queue = _device.getQueue(_queue_family_index, 0);
	_present_queue = _device.getQueue(_queue_family_index, 0);

	// And finally our Command Pool, where we will be creating our command buffers from.
	const auto command_pool_ci = vk::CommandPoolCreateInfo()
		.setFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer | vk::CommandPoolCreateFlagBits::eTransient)
		.setQueueFamilyIndex(_queue_family_index);

	_cmd_pool = _device.createCommandPool(command_pool_ci);
	_descriptor_pool = CreateDescriptorPool();
	_diffuse_sampler = RenderUtils::CreateTextureSampler(_device);
	CreateUniformBuffer();
	_descriptor_set_layout = CreateDescriptorSetLayout();
	_descriptor_set = CreateDescriptorSet(_descriptor_pool, _descriptor_set_layout);
	_graphics_pipeline_layout = CreatePipelineLayout(_descriptor_set_layout);

	CreateSemaphores();
}

VulkanRenderer::~VulkanRenderer()
{
  _device.waitIdle();

  // Allocated in CreateGraphicsPipeline
  for (auto pipeline : _pipelines)
  {
    _device.destroyPipeline(pipeline);
  }

	// Allocated in CreateFramebuffers
	for (auto resources : _swapchain_resources)
	{
		_device.destroyFramebuffer(resources._framebuffer);
	}

	// Allocated in CreateDepthResources
	_device.destroyImageView(_depth_image_view);
	_device.destroyImage(_depth_image);
	_device.freeMemory(_depth_image_mem);

	// Allocated in CreateRenderPass
	_device.destroyRenderPass(_render_pass);

	// Allocated in CreateSwapchain
	for (auto resources : _swapchain_resources)
	{
		_device.destroyImageView(resources._image_view);
		_device.freeCommandBuffers(_cmd_pool, resources._cmd_buffer);
	}
	_device.destroySwapchainKHR(_swapchain);

	// Allocated in AllocateDynamicBuffer
	_aligned_free(_model_matrix_list._buffer);
	_device.destroyBuffer(_dynamic_descriptor._buffer);
	_device.freeMemory(_dynamic_descriptor._buffer_memory);

	// Allocated in LoadModels
	_device.destroyBuffer(_vertex_buffer.first);
	_device.freeMemory(_vertex_buffer.second);
	_device.destroyBuffer(_index_buffer.first);
	_device.freeMemory(_index_buffer.second);

	for (auto model : _models)
	{
		_device.destroyImage(model._diffuse_map._image);
		_device.destroyImageView(model._diffuse_map._image_view);
		_device.freeMemory(model._diffuse_map._image_memory);
	}

	// Allocated in LoadShaders
  for (auto shader : _shaders)
  {
    _device.destroyShaderModule(shader._fragment_shader);
    _device.destroyShaderModule(shader._vertex_shader);
  }

	// Allocated in CreateWin32Surface 
	_instance.destroySurfaceKHR(_surface);

	// Allocated in ctor
	_device.destroySemaphore(_image_available);
	_device.destroySemaphore(_render_finished);
	_device.destroyPipelineLayout(_graphics_pipeline_layout);
	_device.destroyDescriptorSetLayout(_descriptor_set_layout);
	_device.destroyBuffer(_view_descriptor._buffer);
	_device.freeMemory(_view_descriptor._buffer_memory);
  _device.destroyBuffer(_light_descriptor._buffer);
  _device.freeMemory(_light_descriptor._buffer_memory);
  _device.destroySampler(_diffuse_sampler);
	_device.destroyDescriptorPool(_descriptor_pool);
	_device.destroyCommandPool(_cmd_pool);
	_device.destroy();
	_instance.destroy();
}

void VulkanRenderer::CreateWin32Surface(HINSTANCE hinst, HWND hwnd)
{
	const auto surface_ci = vk::Win32SurfaceCreateInfoKHR()
		.setHwnd(hwnd)
		.setHinstance(hinst);

	_surface = _instance.createWin32SurfaceKHR(surface_ci);

	// Is this a surface supported by our GPU?
	if (VK_FALSE == _gpu.getSurfaceSupportKHR(_queue_family_index, _surface))
	{
		throw std::runtime_error("ERROR: Surface is not supported by GPU!");
	}

	// Get the surface format we need to create our swap chain.
	// TODO We are using the first one. Should choose one based on needs.
	auto surface_formats = _gpu.getSurfaceFormatsKHR(_surface);
	_surface_format = surface_formats[0];
}

uint32_t VulkanRenderer::LoadShaders(const std::vector<char> vertex_shader, const std::vector<char> fragment_shader)
{
	const auto vert_shader_ci = vk::ShaderModuleCreateInfo()
		.setPCode(reinterpret_cast<const uint32_t*>(vertex_shader.data()))
		.setCodeSize(vertex_shader.size());

	const auto frag_shader_ci = vk::ShaderModuleCreateInfo()
		.setPCode(reinterpret_cast<const uint32_t*>(fragment_shader.data()))
		.setCodeSize(fragment_shader.size());
  
  ShaderResources shader;

  shader._vertex_shader = _device.createShaderModule(vert_shader_ci);
  shader._fragment_shader = _device.createShaderModule(frag_shader_ci);

  _shaders.push_back(shader);
  return static_cast<uint32_t>(_shaders.size()) - 1;
}

void VulkanRenderer::LoadModels(const std::vector<Model> models)
{
	// For each model in our list, we create an internal version to render later.
	// This includes vertex and index buffer creation as well as loading any and all textures
	// and other uniform data.
	_models.clear();

	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;

	uint32_t vertex_offset = 0;
	uint32_t index_offset = 0;
	for (auto model : models)
	{
		ModelInternal internal_model;
		internal_model._vertex_offset = vertex_offset;
		internal_model._index_offset = index_offset;
		internal_model._vertex_size = static_cast<uint32_t>(model._vertices.size());
		internal_model._index_size = static_cast<uint32_t>(model._indices.size());

    internal_model._shader_index = model._shader_index;
		
		// Load the texture
		internal_model._diffuse_map = LoadTexture(model._diffuse_map, _diffuse_sampler);

		// Append to the larger vertex and index buffers for later copying into our GPU buffer.
		vertices.insert(vertices.end(), model._vertices.begin(), model._vertices.end());
		indices.insert(indices.end(), model._indices.begin(), model._indices.end());

		// Increment the offsets so we know where the next model will begin in our buffer.
		vertex_offset += static_cast<uint32_t>(model._vertices.size());
		index_offset += static_cast<uint32_t>(model._indices.size());

		// And save off the internals for rendering later.
		_models.push_back(internal_model);
	}

	// Now we have complete vertex and index buffers to be packed and sent off to the GPU.
	_vertex_buffer = CreateGeometryBuffer<Vertex>(vertices, vk::BufferUsageFlagBits::eVertexBuffer);
	_index_buffer = CreateGeometryBuffer<uint32_t>(indices, vk::BufferUsageFlagBits::eIndexBuffer);

	// And we can also now allocate the dynamic buffer that houses our model matrices.
	AllocateDynamicBuffer();

	// Lets also intialize those model matrices to identity while we're at it.
	const glm::mat4 identity_matrix(1.0f);
	for (int i = 0; i < models.size(); ++i)
	{
		UpdateModelMatrix(i, identity_matrix);
	}
}

void VulkanRenderer::ClearModelList()
{
  // Free all memory associated with our current model list.
  _device.waitIdle();

  _aligned_free(_model_matrix_list._buffer);
  _device.destroyBuffer(_dynamic_descriptor._buffer);
  _device.freeMemory(_dynamic_descriptor._buffer_memory);

  _device.destroyBuffer(_vertex_buffer.first);
  _device.freeMemory(_vertex_buffer.second);
  _device.destroyBuffer(_index_buffer.first);
  _device.freeMemory(_index_buffer.second);

  for (auto model : _models)
  {
    _device.destroyImage(model._diffuse_map._image);
    _device.destroyImageView(model._diffuse_map._image_view);
    _device.freeMemory(model._diffuse_map._image_memory);
  }

  _models.clear();
}

void VulkanRenderer::PrepareRenderer()
{
	// We assume by now that the vertex/index buffers are ready to go, and all textures are loaded. 
	// We will be creating our swapchain, depth buffer, render passes, framebuffer graphics pipeline and command buffers.
	CreateSwapchain();
	_render_pass = CreateRenderPass();
	CreateDepthResources();
	CreateFramebuffers();
	CreateGraphicsPipelines();
}

void VulkanRenderer::RenderScene()
{
	std::array<vk::ClearValue, 2> clear_values = {};
	clear_values[0].setColor(CLR_BLACK);
	clear_values[1].setDepthStencil(vk::ClearDepthStencilValue(1.0f, 0));

	const auto command_buffer_bi = vk::CommandBufferBeginInfo()
		.setFlags(vk::CommandBufferUsageFlagBits::eSimultaneousUse);

	auto render_pass_bi = vk::RenderPassBeginInfo()
		.setRenderPass(_render_pass)
		.setRenderArea(vk::Rect2D(vk::Offset2D(), _surface_extents))
		.setClearValueCount(static_cast<uint32_t>(clear_values.size()))
		.setPClearValues(clear_values.data());

	// Set up the write descriptor for the UBO. We'll update it for each object to reflect the object materials we're using.
	std::array<vk::WriteDescriptorSet, 4> write_descriptor_set;
	write_descriptor_set[0] = vk::WriteDescriptorSet()
		.setDstSet(_descriptor_set)
		.setDstBinding(0)
		.setDstArrayElement(0)
		.setDescriptorType(vk::DescriptorType::eUniformBuffer)
		.setDescriptorCount(1)
		.setPBufferInfo(&_view_descriptor._buffer_info);
	write_descriptor_set[1] = vk::WriteDescriptorSet()
		.setDstSet(_descriptor_set)
		.setDstBinding(1)
		.setDstArrayElement(0)
		.setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
		.setDescriptorCount(1);
	write_descriptor_set[2] = vk::WriteDescriptorSet()
		.setDstSet(_descriptor_set)
		.setDstBinding(2)
		.setDstArrayElement(0)
		.setDescriptorType(vk::DescriptorType::eUniformBufferDynamic)
		.setDescriptorCount(1)
		.setPBufferInfo(&_dynamic_descriptor._buffer_info);
  write_descriptor_set[3] = vk::WriteDescriptorSet()
    .setDstSet(_descriptor_set)
    .setDstBinding(3)
    .setDstArrayElement(0)
    .setDescriptorType(vk::DescriptorType::eUniformBuffer)
    .setDescriptorCount(1)
    .setPBufferInfo(&_light_descriptor._buffer_info);

	// Fill the command buffers for the render pass.
	for (auto swapchain : _swapchain_resources)
	{
		// Start recording on the command buffer
		swapchain._cmd_buffer.begin(&command_buffer_bi);
		render_pass_bi.setFramebuffer(swapchain._framebuffer);

		// Begin the render pass and bind the pipeline.
		swapchain._cmd_buffer.beginRenderPass(render_pass_bi, vk::SubpassContents::eInline);

    // Bind the vertex and index buffers.
    swapchain._cmd_buffer.bindVertexBuffers(0, _vertex_buffer.first, vk::DeviceSize());
    swapchain._cmd_buffer.bindIndexBuffer(_index_buffer.first, 0, vk::IndexType::eUint32);

    for (uint32_t i = 0; i < _pipelines.size(); ++i)
    {
      swapchain._cmd_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, _pipelines[i]);

      // Now for each model...
      for (uint32_t j = 0; j < _models.size(); ++j)
      {
        // Only render the model if it corresponds to this shader.
        if (_models[j]._shader_index == i)
        {
          // assign its texture to the descriptor.
          write_descriptor_set[1].setPImageInfo(&_models[j]._diffuse_map._image_info);

          // Point to this model's model matrix.
          const uint32_t dynamic_offset = j * static_cast<uint32_t>(_dynamic_buffer_alignment);

          // And update the descriptor set.
          _device.updateDescriptorSets(write_descriptor_set, nullptr);
          swapchain._cmd_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, _graphics_pipeline_layout, 0, _descriptor_set, dynamic_offset);

          // Now draw the indexed primitive using the offsets we snagged earlier.
          swapchain._cmd_buffer.drawIndexed(_models[j]._index_size, 1, _models[j]._index_offset, _models[j]._vertex_offset, 0);
        }
      }
    }
		// Done. end the render pass and stop recording on the command buffer.
		swapchain._cmd_buffer.endRenderPass();
		swapchain._cmd_buffer.end();
	}
}

void VulkanRenderer::Present()
{
  if (_surface_extents.width == 0 || _surface_extents.height == 0)
  {
    // HACK: When our window is minimized we cannot present any images, as Vulkan cannot create framebuffers of size 0.
    return; 
  }
  try
  {
    // Get the image index of the next image to acquire from the swap chain.
    uint32_t image_index;
    auto result = _device.acquireNextImageKHR(_swapchain, (std::numeric_limits<uint64_t>::max)(), _image_available, vk::Fence(nullptr), &image_index);

    const vk::PipelineStageFlags wait_stage_flags = vk::PipelineStageFlagBits::eTopOfPipe;

    const auto submit_info = vk::SubmitInfo()
      .setWaitSemaphoreCount(1)
      .setPWaitSemaphores(&_image_available)
      .setPWaitDstStageMask(&wait_stage_flags)
      .setCommandBufferCount(1)
      .setPCommandBuffers(&_swapchain_resources[image_index]._cmd_buffer)
      .setSignalSemaphoreCount(1)
      .setPSignalSemaphores(&_render_finished);

    _draw_queue.submit(submit_info, vk::Fence(nullptr));

    const auto present_info = vk::PresentInfoKHR()
      .setWaitSemaphoreCount(1)
      .setPWaitSemaphores(&_render_finished)
      .setSwapchainCount(1)
      .setPSwapchains(&_swapchain)
      .setPImageIndices(&image_index);

    result = _present_queue.presentKHR(present_info);
  }
  catch(vk::OutOfDateKHRError &error)
	{
		// The swap chain must be recreated.
    std::cout << error.what() << std::endl;
		HandleResize();
	}
}

void VulkanRenderer::HandleResize()
{
	// In order to resize the window, we must recreate the swapchain and rebuild the command buffers.
	// Wait for the device to be idle before continuing...
	_device.waitIdle();

	// Clean up our swapchain resources...
  for (auto pipeline : _pipelines)
  {
    _device.destroyPipeline(pipeline);
  }

  _pipelines.clear();

	_device.destroyImageView(_depth_image_view);
	_device.destroyImage(_depth_image);
	_device.freeMemory(_depth_image_mem);
	
	_device.destroyRenderPass(_render_pass);

	for (auto resources : _swapchain_resources)
	{
		_device.destroyFramebuffer(resources._framebuffer);
		_device.destroyImageView(resources._image_view);
		_device.freeCommandBuffers(_cmd_pool, resources._cmd_buffer);
	}

	// Now we prepare our Renderer once again.
	PrepareRenderer();
}

void VulkanRenderer::UpdateCameraDescriptor(const CameraDescriptor & ubo)
{
	void* data = _device.mapMemory(_view_descriptor._buffer_memory, 0, sizeof(CameraDescriptor));
	memcpy(data, &ubo, sizeof(CameraDescriptor));
	_device.unmapMemory(_view_descriptor._buffer_memory);
}

void VulkanRenderer::UpdateModelMatrix(uint32_t model_id, const glm::mat4 & model_matrix)
{
	// Offset into the dynamic buffer based on the model ID passed in.
	glm::mat4* model_mat = reinterpret_cast<glm::mat4*>(((uint64_t)_model_matrix_list._buffer + (model_id * _dynamic_buffer_alignment)));
	*model_mat = model_matrix;

	// Now update the dynamic range.
	void* data = _device.mapMemory(_dynamic_descriptor._buffer_memory, model_id * _dynamic_buffer_alignment, _dynamic_buffer_alignment);
	memcpy(data, model_mat, _dynamic_buffer_alignment);
	_device.unmapMemory(_dynamic_descriptor._buffer_memory);

	// Flush to make changes visible to the host 
	const auto memory_range = vk::MappedMemoryRange()
		.setMemory(_dynamic_descriptor._buffer_memory)
		.setOffset(model_id * _dynamic_buffer_alignment)
		.setSize(VK_WHOLE_SIZE);
	_device.flushMappedMemoryRanges(memory_range);
}

void VulkanRenderer::UpdateLightDescriptor(const LightDescriptor & light)
{
  void* data = _device.mapMemory(_light_descriptor._buffer_memory, 0, sizeof(LightDescriptor));
  memcpy(data, &light, sizeof(LightDescriptor));
  _device.unmapMemory(_light_descriptor._buffer_memory);
}

void VulkanRenderer::ClearShaders()
{
  _device.waitIdle();
  for (auto shader : _shaders)
  {
    _device.destroyShaderModule(shader._fragment_shader);
    _device.destroyShaderModule(shader._vertex_shader);
  }
  _shaders.clear();
}

/*****************************************************************************************************/

#pragma region "Private Functions"
vk::Instance VulkanRenderer::CreateVulkanInstance(const char * application_name, uint32_t application_version) const
{
	const auto app_info = vk::ApplicationInfo()
		.setPApplicationName(application_name)
		.setApplicationVersion(application_version)
		.setApiVersion(VK_API_VERSION_1_0);

	const auto instance_info = vk::InstanceCreateInfo()
		.setPApplicationInfo(&app_info)
#ifdef _DEBUG
		// Enable LunarG's standard validation layer which outputs debugging information to the console.
		.setEnabledLayerCount(instance_layer_count)
		.setPpEnabledLayerNames(instance_layer_names)
#endif
		.setEnabledExtensionCount(instance_extension_count)
		.setPpEnabledExtensionNames(instance_extension_names);

	return vk::createInstance(instance_info);
}

vk::Device VulkanRenderer::CreateLogicalDevice()
{
	// We need to find a queue family that supports graphics.
	auto queue_family_props = _gpu.getQueueFamilyProperties();
	_queue_family_index = UINT32_MAX;
	for (auto i = 0; i < queue_family_props.size(); ++i)
	{
		// The queue family must support Graphics Presentation and the ability to present to a Win32 surface.
		if (queue_family_props[i].queueFlags & vk::QueueFlagBits::eGraphics && _gpu.getWin32PresentationSupportKHR(i))
		{
			_queue_family_index = i;
			break;
		}
	}
	if (UINT32_MAX == _queue_family_index)
	{
		throw std::runtime_error("ERROR: No device found that supports graphics and/or Win32!");
	}

	float max_prio = 1.0f;
	const auto queue_create_info = vk::DeviceQueueCreateInfo()
		.setQueueFamilyIndex(_queue_family_index)
		.setQueueCount(1)
		.setPQueuePriorities(&max_prio);

	// We specify our required and optional features. Anything listed as REQUIRED that isn't on the physical device
	// will cause the creation of our logical device to fail.
	// OPTIONAL: Multi-draw Indirect
	// REQUIRED: Tessellation Shader, Geometry Shader, Anisotropic sampling.
	vk::PhysicalDeviceFeatures req_features = vk::PhysicalDeviceFeatures()
		.setMultiDrawIndirect(_gpu.getFeatures().multiDrawIndirect)
		.setTessellationShader(VK_TRUE)
		.setGeometryShader(VK_TRUE)
		.setSamplerAnisotropy(VK_TRUE);

	const auto device_create_info = vk::DeviceCreateInfo()
		.setQueueCreateInfoCount(1)
		.setPQueueCreateInfos(&queue_create_info)
		.setPEnabledFeatures(&req_features)
		.setEnabledExtensionCount(device_extension_count)
		.setPpEnabledExtensionNames(device_extension_names);
	
	return _gpu.createDevice(device_create_info);
}

vk::DescriptorPool VulkanRenderer::CreateDescriptorPool() const
{
	std::array<vk::DescriptorPoolSize, 3> pool_size;
	pool_size[0].setType(vk::DescriptorType::eUniformBuffer);
	pool_size[0].setDescriptorCount(2);
	pool_size[1].setType(vk::DescriptorType::eCombinedImageSampler);
	pool_size[1].setDescriptorCount(1);
	pool_size[2].setType(vk::DescriptorType::eUniformBufferDynamic);
	pool_size[2].setDescriptorCount(1);

	const auto pool_ci = vk::DescriptorPoolCreateInfo()
		.setPoolSizeCount(static_cast<uint32_t>(pool_size.size()))
		.setPPoolSizes(pool_size.data())
		.setMaxSets(1);

	return _device.createDescriptorPool(pool_ci);
}

void VulkanRenderer::CreateUniformBuffer()
{
	auto uniform_buffer = RenderUtils::CreateBuffer(_device, _gpu, sizeof(CameraDescriptor), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
  _view_descriptor._buffer = uniform_buffer.first;
  _view_descriptor._buffer_memory = uniform_buffer.second;

  _view_descriptor._buffer_info = vk::DescriptorBufferInfo()
		.setBuffer(_view_descriptor._buffer)
		.setOffset(0)
		.setRange(sizeof(CameraDescriptor));

  auto light_buffer = RenderUtils::CreateBuffer(_device, _gpu, sizeof(LightDescriptor), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
  _light_descriptor._buffer = light_buffer.first;
  _light_descriptor._buffer_memory = light_buffer.second;

  _light_descriptor._buffer_info = vk::DescriptorBufferInfo()
    .setBuffer(_light_descriptor._buffer)
    .setOffset(0)
    .setRange(sizeof(LightDescriptor));
}

vk::DescriptorSetLayout VulkanRenderer::CreateDescriptorSetLayout() const
{
	const auto ubo_layout_binding = vk::DescriptorSetLayoutBinding()
		.setBinding(0)
		.setDescriptorType(vk::DescriptorType::eUniformBuffer)
		.setDescriptorCount(1)
		.setStageFlags(vk::ShaderStageFlagBits::eVertex)
		.setPImmutableSamplers(nullptr);

	const auto sampler_layout_binding = vk::DescriptorSetLayoutBinding()
		.setBinding(1)
		.setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
		.setDescriptorCount(1)
		.setStageFlags(vk::ShaderStageFlagBits::eFragment)
		.setPImmutableSamplers(nullptr);

	const auto dynamic_layout_binding = vk::DescriptorSetLayoutBinding()
		.setBinding(2)
		.setDescriptorType(vk::DescriptorType::eUniformBufferDynamic)
		.setDescriptorCount(1)
		.setStageFlags(vk::ShaderStageFlagBits::eVertex)
		.setPImmutableSamplers(nullptr);

  const auto light_layout_binding = vk::DescriptorSetLayoutBinding()
    .setBinding(3)
    .setDescriptorType(vk::DescriptorType::eUniformBuffer)
    .setDescriptorCount(1)
    .setStageFlags(vk::ShaderStageFlagBits::eVertex)
    .setPImmutableSamplers(nullptr);

	std::array<vk::DescriptorSetLayoutBinding, 4> descriptor_sets = { ubo_layout_binding, sampler_layout_binding, dynamic_layout_binding, light_layout_binding };

	const auto layout_ci = vk::DescriptorSetLayoutCreateInfo()
		.setBindingCount(static_cast<uint32_t>(descriptor_sets.size()))
		.setPBindings(descriptor_sets.data());

	return _device.createDescriptorSetLayout(layout_ci);
}

vk::DescriptorSet VulkanRenderer::CreateDescriptorSet(vk::DescriptorPool descriptor_pool, vk::DescriptorSetLayout descriptor_set_layout) const
{
	const auto descriptor_set_ai = vk::DescriptorSetAllocateInfo()
		.setDescriptorPool(descriptor_pool)
		.setDescriptorSetCount(1)
		.setPSetLayouts(&descriptor_set_layout);

	return _device.allocateDescriptorSets(descriptor_set_ai)[0];
}

vk::PipelineLayout VulkanRenderer::CreatePipelineLayout(vk::DescriptorSetLayout descriptor_set_layout) const
{
	const auto pipeline_layout_ci = vk::PipelineLayoutCreateInfo()
		.setSetLayoutCount(1)
		.setPSetLayouts(&descriptor_set_layout)
		.setPushConstantRangeCount(0)
		.setPPushConstantRanges(nullptr);

	return _device.createPipelineLayout(pipeline_layout_ci);
}

void VulkanRenderer::CreateSemaphores()
{
	const auto semaphore_ci = vk::SemaphoreCreateInfo();

	_image_available = _device.createSemaphore(semaphore_ci);
	_render_finished = _device.createSemaphore(semaphore_ci);
}

VulkanRenderer::TextureResources VulkanRenderer::LoadTexture(const Texture & texture, vk::Sampler sampler) const
{
	// Create a temporary buffer to stage the texture.
	const auto image_size = vk::DeviceSize(texture.width * texture.height * texture.channels);
	const auto buffer = RenderUtils::CreateBuffer(_device, _gpu, image_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
	
	// Copy data.
	void* data = _device.mapMemory(buffer.second, 0, image_size);
	memcpy(data, texture.pixels, image_size);
	_device.unmapMemory(buffer.second);

	// Pixel values are now stored on our staging buffer. Push to a new texture.
	const auto image = RenderUtils::CreateImage(_device, _gpu, texture.width, texture.height, vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal);
	RenderUtils::TransitionImageLayout(_device, _cmd_pool, _draw_queue, image.first, vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);
	RenderUtils::CopyBufferToImage(_device, _cmd_pool, _draw_queue, buffer.first, image.first, texture.width, texture.height);
	RenderUtils::TransitionImageLayout(_device, _cmd_pool, _draw_queue, image.first, vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);

	TextureResources tex_resources;
	tex_resources._image = image.first;
	tex_resources._image_memory = image.second;

	// Create a view for the image.
	tex_resources._image_view = RenderUtils::CreateImageView(_device, image.first, vk::Format::eR8G8B8A8Unorm, vk::ImageAspectFlagBits::eColor);

	// And image_info for referencing when drawing
	tex_resources._image_info = vk::DescriptorImageInfo()
		.setImageView(tex_resources._image_view)
		.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
		.setSampler(sampler);

	// Clean up the staging buffer.
	_device.destroyBuffer(buffer.first);
	_device.freeMemory(buffer.second);

	return tex_resources;
}

void VulkanRenderer::AllocateDynamicBuffer()
{
	// Calculate required alignment based on minimum device offset alignment
	const auto min_alignment = _gpu_props.limits.minUniformBufferOffsetAlignment;
	_dynamic_buffer_alignment = sizeof(glm::mat4);
	
	if (min_alignment > 0) 
	{
		_dynamic_buffer_alignment = static_cast<uint32_t>((_dynamic_buffer_alignment + min_alignment - 1) & ~(min_alignment - 1));
	}

	const auto buffer_size = _models.size() * _dynamic_buffer_alignment;
	_model_matrix_list._buffer = reinterpret_cast<glm::mat4*>(_aligned_malloc(buffer_size, _dynamic_buffer_alignment));

	const auto dynamic_buffer = RenderUtils::CreateBuffer(_device, _gpu, buffer_size, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible);
  _dynamic_descriptor._buffer = dynamic_buffer.first;
  _dynamic_descriptor._buffer_memory = dynamic_buffer.second;

  _dynamic_descriptor._buffer_info = vk::DescriptorBufferInfo()
		.setBuffer(_dynamic_descriptor._buffer)
		.setOffset(0)
		.setRange(buffer_size);
}

void VulkanRenderer::CreateSwapchain()
{
	// Get the current window size from the surface capabilities
	const auto surface_caps = _gpu.getSurfaceCapabilitiesKHR(_surface);
	_surface_extents = surface_caps.currentExtent;

	// Create the swapchain.
	vk::SwapchainKHR old_swapchain = _swapchain;
	const auto swapchain_ci = vk::SwapchainCreateInfoKHR()
		.setSurface(_surface)
		.setMinImageCount(NUM_IMAGES)
		.setImageFormat(_surface_format.format)
		.setImageColorSpace(_surface_format.colorSpace)
		.setImageExtent(surface_caps.currentExtent)
		.setImageArrayLayers(1)
		.setImageUsage(vk::ImageUsageFlagBits::eColorAttachment)
		.setImageSharingMode(vk::SharingMode::eExclusive)
		.setPreTransform(vk::SurfaceTransformFlagBitsKHR::eIdentity)
		.setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque)
		.setPresentMode(vk::PresentModeKHR::eFifo)
		.setClipped(VK_FALSE)
		.setOldSwapchain(old_swapchain);

	_swapchain = _device.createSwapchainKHR(swapchain_ci);

	// Destroy the old swapchain.
	if (old_swapchain)
	{
		_device.destroySwapchainKHR(old_swapchain);
	}

	// Get the images for our swapchain and allocate space in the vector.
	auto swapchain_images = _device.getSwapchainImagesKHR(_swapchain);
	_swapchain_resources.resize(swapchain_images.size());

	// Set up the alloc info for the command buffers
	const auto command_buffer_ai = vk::CommandBufferAllocateInfo()
		.setCommandPool(_cmd_pool)
		.setLevel(vk::CommandBufferLevel::ePrimary)
		.setCommandBufferCount(1);

	// Now fill in the swapchain images and image views, as well as the command buffer.
	for (uint32_t i = 0; i < swapchain_images.size(); ++i)
	{
		_swapchain_resources[i]._image = swapchain_images[i];

		const auto image_view_ci = vk::ImageViewCreateInfo()
			.setViewType(vk::ImageViewType::e2D)
			.setFormat(_surface_format.format)
			.setSubresourceRange(vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1))
			.setImage(swapchain_images[i]);

		_swapchain_resources[i]._image_view = _device.createImageView(image_view_ci);
		_swapchain_resources[i]._cmd_buffer = _device.allocateCommandBuffers(command_buffer_ai)[0];
	}
}

vk::RenderPass VulkanRenderer::CreateRenderPass() const
{
	// Our render pass contains a single subpass and two attachments: One for the color and one for depth/stencil.
	const auto color_attach_desc = vk::AttachmentDescription()
		.setFormat(_surface_format.format)
		.setSamples(vk::SampleCountFlagBits::e1)
		.setLoadOp(vk::AttachmentLoadOp::eClear)
		.setStoreOp(vk::AttachmentStoreOp::eStore)
		.setStencilLoadOp(vk::AttachmentLoadOp::eDontCare)
		.setStencilStoreOp(vk::AttachmentStoreOp::eDontCare)
		.setInitialLayout(vk::ImageLayout::eUndefined)
		.setFinalLayout(vk::ImageLayout::ePresentSrcKHR);

	const auto depth_attach_desc = vk::AttachmentDescription()
		.setFormat(vk::Format::eD32SfloatS8Uint)
		.setSamples(vk::SampleCountFlagBits::e1)
		.setLoadOp(vk::AttachmentLoadOp::eClear)
		.setStoreOp(vk::AttachmentStoreOp::eDontCare)
		.setStencilLoadOp(vk::AttachmentLoadOp::eDontCare)
		.setStencilStoreOp(vk::AttachmentStoreOp::eDontCare)
		.setInitialLayout(vk::ImageLayout::eUndefined)
		.setFinalLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);

	std::array<vk::AttachmentDescription, 2> attachments = { color_attach_desc, depth_attach_desc };

	const auto color_attach_ref = vk::AttachmentReference()
		.setAttachment(0)
		.setLayout(vk::ImageLayout::eColorAttachmentOptimal);

	const auto depth_attach_ref = vk::AttachmentReference()
		.setAttachment(1)
		.setLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);

	const auto subpass_desc = vk::SubpassDescription()
		.setPipelineBindPoint(vk::PipelineBindPoint::eGraphics)
		.setColorAttachmentCount(1)
		.setPColorAttachments(&color_attach_ref)
		.setPDepthStencilAttachment(&depth_attach_ref);

	const auto render_pass_ci = vk::RenderPassCreateInfo()
		.setAttachmentCount(static_cast<uint32_t>(attachments.size()))
		.setPAttachments(attachments.data())
		.setSubpassCount(1)
		.setPSubpasses(&subpass_desc);

	return _device.createRenderPass(render_pass_ci);
}

void VulkanRenderer::CreateDepthResources()
{
	auto depth_image_resources = RenderUtils::CreateImage(_device, _gpu, _surface_extents.width, _surface_extents.height, vk::Format::eD32SfloatS8Uint, 
		vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);

	_depth_image = depth_image_resources.first;
	_depth_image_mem = depth_image_resources.second;
	_depth_image_view = RenderUtils::CreateImageView(_device, _depth_image, vk::Format::eD32SfloatS8Uint, vk::ImageAspectFlagBits::eDepth);

	RenderUtils::TransitionImageLayout(_device, _cmd_pool, _draw_queue, _depth_image, vk::Format::eD32SfloatS8Uint, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal);
}

void VulkanRenderer::CreateFramebuffers()
{
	for (auto i = 0; i < _swapchain_resources.size(); ++i)
	{
		std::array<vk::ImageView, 2> attachments = { _swapchain_resources[i]._image_view, _depth_image_view };

		const auto framebuffer_ci = vk::FramebufferCreateInfo()
			.setRenderPass(_render_pass)
			.setPAttachments(attachments.data())
			.setAttachmentCount(static_cast<uint32_t>(attachments.size()))
			.setWidth(_surface_extents.width)
			.setHeight(_surface_extents.height)
			.setLayers(1);

		_swapchain_resources[i]._framebuffer = _device.createFramebuffer(framebuffer_ci);
	}
}

void VulkanRenderer::CreateGraphicsPipelines()
{
  for (auto shader : _shaders)
  {
    const auto vertex_stage_ci = vk::PipelineShaderStageCreateInfo()
      .setStage(vk::ShaderStageFlagBits::eVertex)
      .setModule(shader._vertex_shader)
      .setPName("main");

    const auto fragment_stage_ci = vk::PipelineShaderStageCreateInfo()
      .setStage(vk::ShaderStageFlagBits::eFragment)
      .setModule(shader._fragment_shader)
      .setPName("main");

    const std::array<const vk::PipelineShaderStageCreateInfo, 2> shader_stages = { vertex_stage_ci, fragment_stage_ci };

    const auto vertex_binding = vk::VertexInputBindingDescription()
      .setBinding(0)
      .setStride(sizeof(Vertex))
      .setInputRate(vk::VertexInputRate::eVertex);

    const auto pos_attrib = vk::VertexInputAttributeDescription(0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, pos));
    const auto normal_attrib = vk::VertexInputAttributeDescription(1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal));
    const auto texcoord_attrib = vk::VertexInputAttributeDescription(2, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, tex_coord));

    const std::array<const vk::VertexInputAttributeDescription, 3> vert_attribs = { pos_attrib, normal_attrib, texcoord_attrib };

    const auto vert_input_state_ci = vk::PipelineVertexInputStateCreateInfo()
      .setVertexBindingDescriptionCount(1)
      .setPVertexBindingDescriptions(&vertex_binding)
      .setVertexAttributeDescriptionCount(static_cast<uint32_t>(vert_attribs.size()))
      .setPVertexAttributeDescriptions(vert_attribs.data());

    const auto input_assembly_state_ci = vk::PipelineInputAssemblyStateCreateInfo()
      .setTopology(vk::PrimitiveTopology::eTriangleList)
      .setPrimitiveRestartEnable(VK_FALSE);

    const auto viewport = vk::Viewport(0.0f, 0.0f, static_cast<float>(_surface_extents.width), static_cast<float>(_surface_extents.height), 0.1f, 1.0f);
    const auto scissor = vk::Rect2D(vk::Offset2D(0, 0), _surface_extents);

    const auto viewport_state_ci = vk::PipelineViewportStateCreateInfo()
      .setViewportCount(1)
      .setPViewports(&viewport)
      .setScissorCount(1)
      .setPScissors(&scissor);

    const auto raster_state_ci = vk::PipelineRasterizationStateCreateInfo()
      .setDepthClampEnable(VK_FALSE)
      .setRasterizerDiscardEnable(VK_FALSE)
      .setPolygonMode(vk::PolygonMode::eFill)
      .setLineWidth(1.0f)
      .setCullMode(vk::CullModeFlagBits::eBack)
      .setFrontFace(vk::FrontFace::eCounterClockwise)
      .setDepthBiasEnable(VK_FALSE);

    const auto multisample_state_ci = vk::PipelineMultisampleStateCreateInfo()
      .setSampleShadingEnable(VK_FALSE)
      .setRasterizationSamples(vk::SampleCountFlagBits::e1);

    const auto color_blend_attachment = vk::PipelineColorBlendAttachmentState()
      .setBlendEnable(VK_TRUE)
      .setColorWriteMask(vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA)
      .setColorBlendOp(vk::BlendOp::eAdd)
      .setSrcColorBlendFactor(vk::BlendFactor::eSrcAlpha)
      .setDstColorBlendFactor(vk::BlendFactor::eOneMinusSrcAlpha)
      .setAlphaBlendOp(vk::BlendOp::eAdd)
      .setSrcAlphaBlendFactor(vk::BlendFactor::eOne)
      .setDstAlphaBlendFactor(vk::BlendFactor::eZero);

    const auto color_blend_state_ci = vk::PipelineColorBlendStateCreateInfo()
      .setLogicOpEnable(VK_FALSE)
      .setLogicOp(vk::LogicOp::eCopy)
      .setAttachmentCount(1)
      .setPAttachments(&color_blend_attachment)
      .setBlendConstants({ 1.0f, 1.0f, 1.0f, 1.0f });

    const auto depth_stencil_state_ci = vk::PipelineDepthStencilStateCreateInfo()
      .setDepthTestEnable(VK_TRUE)
      .setDepthWriteEnable(VK_TRUE)
      .setDepthCompareOp(vk::CompareOp::eLess)
      .setDepthBoundsTestEnable(VK_FALSE)
      .setMinDepthBounds(0.0f)
      .setMaxDepthBounds(1.0f)
      .setStencilTestEnable(VK_FALSE);

    const auto pipeline_ci = vk::GraphicsPipelineCreateInfo()
      .setStageCount(2)
      .setPStages(shader_stages.data())
      .setPVertexInputState(&vert_input_state_ci)
      .setPInputAssemblyState(&input_assembly_state_ci)
      .setPViewportState(&viewport_state_ci)
      .setPRasterizationState(&raster_state_ci)
      .setPMultisampleState(&multisample_state_ci)
      .setPColorBlendState(&color_blend_state_ci)
      .setPDepthStencilState(&depth_stencil_state_ci)
      .setLayout(_graphics_pipeline_layout)
      .setRenderPass(_render_pass)
      .setSubpass(0)
      .setPDynamicState(nullptr);

    const auto pipeline_cache_ci = vk::PipelineCacheCreateInfo();
    auto pipeline = _device.createGraphicsPipeline(vk::PipelineCache(nullptr), pipeline_ci);
    _pipelines.push_back(pipeline.value);
  }
}

template<typename T>
std::pair<vk::Buffer, vk::DeviceMemory> VulkanRenderer::CreateGeometryBuffer(const std::vector<T>& data, vk::BufferUsageFlagBits buffer_type) const
{
	const auto buffer_size = vk::DeviceSize(sizeof(data[0]) * data.size());
	const auto num_items = static_cast<uint32_t>(data.size());

	const auto staging_buffer = RenderUtils::CreateBuffer(_device, _gpu, buffer_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

	void* data_block = _device.mapMemory(staging_buffer.second, 0, buffer_size);
	memcpy(data_block, data.data(), buffer_size);
	_device.unmapMemory(staging_buffer.second);

	const auto data_buffer = RenderUtils::CreateBuffer(_device, _gpu, buffer_size, buffer_type | vk::BufferUsageFlagBits::eTransferDst, vk::MemoryPropertyFlagBits::eDeviceLocal);
	RenderUtils::CopyBuffer(_device, _cmd_pool, _draw_queue, staging_buffer.first, data_buffer.first, buffer_size);

	_device.destroyBuffer(staging_buffer.first);
	_device.freeMemory(staging_buffer.second);

	return data_buffer;
}
#pragma endregion

std::ostream& operator<<(std::ostream& out, const VulkanRenderer& renderer)
{
	out << "Selected Device: " << renderer._gpu_props.deviceName << std::endl;
	return out;
}
