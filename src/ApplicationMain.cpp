#include <iostream>
#include <fstream>
#include <tchar.h>
#include <chrono>
#include <unordered_map>

#include "Win32App.h"
#include "VulkanRenderer.h"
#include "Model.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "stb_image.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

// Global Constants
namespace
{
  const char* Dwarf_Model = "./models/Dwarf.obj";
  const char* Dwarf_Texture = "./textures/Dwarf.jpg";
  const char* Pokemon_Model = "./models/ivysaur.obj";
  const char* Pokemon_Texture = "./textures/ivysaurD.jpg";
  const char* Cube_Model = "./models/cube.obj";
  const char* Cube_Texture = "./textures/cube.png";
  const char* Floor_Model = "./models/Floor.obj";
  const char* Floor_Texture = "./textures/floor.png";

  const char* light_vert_shader_name = "./shaders/SPIR/light_vert.spv";
  const char* light_frag_shader_name = "./shaders/SPIR/light_frag.spv";

  const char* bb_vert_shader_name = "./shaders/SPIR/bb_vert.spv";
  const char* bb_frag_shader_name = "./shaders/SPIR/bb_frag.spv";
}

// Helper function prototypes.
Model LoadModel(const char* model_path, const char* texture_path);
Model LoadBillboardModel();
bool LoadShaderFromFile(const char* shader_file, std::vector<char> &shader);

// Super simple helper class for the camera.
class OrbitCamera
{
private:
	glm::vec3 _eye_pos = { 0.0f, 0.0f, 0.0f };
	glm::vec3 _look_at = { 0.0f, 0.0f, 0.0f };
	glm::vec3 _up_vec = { 0.0f, 1.0f, 0.0f };

	float _velocity_radians_per_pixel = 0.01f / 1.0f;
	float _phi = 3.14f / 2.0f;
	float _theta = 0.0f;
	float _radius = 2.0f;

public:
	void IncrementRadius(float delta)
	{
		_radius = (_radius + delta > 0.0f ? _radius + delta : _radius / 2.0f);
	}

	void SetEyePosition(float x, float y, float z)
	{
		_eye_pos = glm::vec3(x, y, z);
	}

	void SetLookAtPosition(float x, float y, float z)
	{
		_look_at = glm::vec3(x, y, z);
	}

	void SetNewAnglesFromScreenSpace(int32_t x_delta_pixels, int32_t y_delta_pixels)
	{
		_phi -= y_delta_pixels * _velocity_radians_per_pixel;
		_theta += x_delta_pixels * _velocity_radians_per_pixel;

		// Error check phi; matching the up_vec will destroy our view matrix.
		if (_phi > 3.13f)
		{
			_phi = 3.13f;
		}
		else if (_phi < 0.01f)
		{
			_phi = 0.01f;
		}
	}

	glm::mat4 GetViewMatrix()
	{
    float x_pos = _eye_pos.x +(_radius * sin(_phi) * cos(_theta));
    float y_pos = _eye_pos.y +(_radius * cos(_phi));
    float z_pos = _eye_pos.z +(_radius * sin(_phi) * sin(_theta));

		return glm::lookAt(glm::vec3(x_pos, y_pos, z_pos), _look_at, _up_vec);
	}
};

int main(int argc, char** argv)
{
  // Read shaders from their text files.
  std::vector<char> light_vert_shader, light_frag_shader, bb_vert_shader, bb_frag_shader;
  LoadShaderFromFile(light_vert_shader_name, light_vert_shader);
  LoadShaderFromFile(light_frag_shader_name, light_frag_shader);
  LoadShaderFromFile(bb_vert_shader_name, bb_vert_shader);
  LoadShaderFromFile(bb_frag_shader_name, bb_frag_shader);

	Win32App appWindow;
	VulkanRenderer renderer("Fun with Vulkan: Lighting", 1);

	uint16_t width = 800, height = 600;
	OrbitCamera camera;
  camera.SetEyePosition(0.0f, 2.0f, -1.0f);
  camera.SetLookAtPosition(0.0f, 1.0f, 1.0f);
  VulkanRenderer::LightDescriptor light_desc;
  light_desc._light_color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
  float degrees_per_second = 90.0f;
  float light_radius = 3.0f;
  uint32_t x_pos = 0, y_pos = 0;
  bool tracking = false;

	VulkanRenderer::CameraDescriptor camera_descriptor;
  camera_descriptor._view = camera.GetViewMatrix();
	camera_descriptor._projection = glm::perspective(glm::radians(45.0f), static_cast<float>(width) / static_cast<float>(height), 0.1f, 1000.0f);
	camera_descriptor._projection[1][1] *= -1.0f;	// Secret sauce for converting an OpenGL Projection matrix to Vulkan projection (-y up direction)

	appWindow.CreateAppWindow("MyVulkanClass", "Vulkan Test Application", 800, 600);
	
	appWindow.SetOnKeyupCb([&](uint32_t key)
	{
		if (key == VK_RETURN)
		{
			std::cout << "Enter Pressed!" << std::endl;
			// Recompile the shaders.
      renderer.ClearShaders();

      light_vert_shader.clear();
      light_frag_shader.clear();
      bb_vert_shader.clear();
      bb_frag_shader.clear();
      LoadShaderFromFile(light_vert_shader_name, light_vert_shader);
      LoadShaderFromFile(light_frag_shader_name, light_frag_shader);
      LoadShaderFromFile(bb_vert_shader_name, bb_vert_shader);
      LoadShaderFromFile(bb_frag_shader_name, bb_frag_shader);

			renderer.LoadShaders(light_vert_shader, light_frag_shader);
      renderer.LoadShaders(bb_vert_shader, bb_frag_shader);

			// And rebuild the graphics pipelines.
			renderer.HandleResize();
			renderer.RenderScene();
		}
    else if (key == '1')
    {
      light_desc._light_color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    }
    else if (key == '2')
    {
      light_desc._light_color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
    }
    else if (key == '3')
    {
      light_desc._light_color = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
    }
    else if (key == '4')
    {
      light_desc._light_color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
    }
    else if (key == '5')
    {
      light_desc._light_color = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
    }
    else if (key == '6')
    {
      light_desc._light_color = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
    }
    else if (key == '7')
    {
      light_desc._light_color = glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
    }
    else if (key == '8')
    {
      light_desc._light_color = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
    }
    else if (key == '9')
    {
      light_desc._light_color = glm::vec4(0.25f, 0.25f, 0.25f, 1.0f);
    }
    else if (key == VK_OEM_PLUS)
    {
      light_radius += 0.2f;
    }
    else if (key == VK_OEM_MINUS)
    {
      light_radius -= 0.2f;
    }
	});

	appWindow.SetResizeCb([&](uint16_t w, uint16_t h)
	{
    if (w > 0 && h > 0)
    {
      // If the window size changes the swap chain is invalidated and must be recreated.
      renderer.HandleResize();
      renderer.RenderScene();
      width = w;
      height = h;
      camera_descriptor._projection = glm::perspective(glm::radians(45.0f), static_cast<float>(width) / static_cast<float>(height), 0.1f, 1000.0f);
      camera_descriptor._projection[1][1] *= -1.0f;
      renderer.UpdateCameraDescriptor(camera_descriptor);
    }
	});

	appWindow.SetMouseMoveCb([&](uint16_t x, uint16_t y)
	{
		if (tracking)
		{
			int32_t delta_x = x - x_pos;
			int32_t delta_y = y - y_pos;

			camera.SetNewAnglesFromScreenSpace(delta_x, delta_y);
			camera_descriptor._view = camera.GetViewMatrix();
			renderer.UpdateCameraDescriptor(camera_descriptor);

			x_pos = x;
			y_pos = y;
		}
	} );

	appWindow.SetLeftButtonDownCb([&](uint16_t x, uint16_t y)
	{
		tracking = true;
		x_pos = x;
		y_pos = y;
	} );

	appWindow.SetLeftButtonUpCb([&tracking]()
	{
		tracking = false;
	} );

	appWindow.SetMouseWheelCb([&](int32_t wheel_val, int32_t wheel_delta) 
	{
		float radius_delta = static_cast<float>(wheel_val / wheel_delta) * -0.1f; // Down should move us backward (increase radius)
		camera.IncrementRadius(radius_delta);
		camera_descriptor._view = camera.GetViewMatrix();
		renderer.UpdateCameraDescriptor(camera_descriptor);
	} );

	renderer.CreateWin32Surface(appWindow.GetInstance(), appWindow.GetHWND());

  // Load shaders into the renderer.
  auto lighting_shader_index = renderer.LoadShaders(light_vert_shader, light_frag_shader);
  auto bb_shader_index = renderer.LoadShaders(bb_vert_shader, bb_frag_shader);

  // Load the models
  Model pokemon = LoadModel(Pokemon_Model, Pokemon_Texture);
  Model floor = LoadModel(Floor_Model, Floor_Texture);
  Model light = LoadBillboardModel();
  pokemon._shader_index = lighting_shader_index;
  floor._shader_index =  lighting_shader_index;
  light._shader_index = bb_shader_index;

  // TODO this isn't a super effective use of our vertex buffer. We should be instancing this model instead of loading it three times.
  // This could be handled entirely on the renderer side if it detects duplication of vertex/index data, but for this demo it will suffice.
  std::vector<Model> models({ pokemon, pokemon, pokemon, floor, light });
  renderer.LoadModels(models);

  // No longer need the image data in memory.
  stbi_image_free(pokemon._diffuse_map.pixels);
  stbi_image_free(floor._diffuse_map.pixels);
  stbi_image_free(light._diffuse_map.pixels);

  // Complete the rendering pipeline.
	renderer.PrepareRenderer();
	renderer.UpdateCameraDescriptor(camera_descriptor);

  // Move the models into new positions.
  glm::mat4 model_transform = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
  renderer.UpdateModelMatrix(0, model_transform);
  model_transform = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, 0.0f, 0.0f)) * glm::rotate(glm::mat4(1.0f), -45.0f, glm::vec3(0.0f, 1.0f, 0.0f));
  renderer.UpdateModelMatrix(1, model_transform);
  model_transform = glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, 0.0f, 0.0f)) * glm::rotate(glm::mat4(1.0f), 45.0f, glm::vec3(0.0f, 1.0f, 0.0f));
  renderer.UpdateModelMatrix(2, model_transform);

  // Set the light position.
  light_desc._light_position = glm::vec4(0.0f, 1.5f, 3.0f, 1.0f);
  model_transform = glm::translate(glm::mat4(1.0f), glm::vec3(light_desc._light_position));
  renderer.UpdateModelMatrix(4, model_transform);
  renderer.UpdateLightDescriptor(light_desc);

  // Render the scene.
  renderer.RenderScene();

  auto start_time = std::chrono::high_resolution_clock::now();
  auto last_time = start_time;
  float angle = 0.0f;

  // Start the main loop.
	while (appWindow.ProcessUpdate())
	{
    // Get Delta t.
    auto current_time = std::chrono::high_resolution_clock::now();
    auto delta_t = std::chrono::duration<float, std::chrono::seconds::period>(current_time - last_time).count();

    angle += (delta_t * glm::radians(degrees_per_second));

    // Update light position
    light_desc._light_position.x = light_radius * sin(angle);
    light_desc._light_position.y = 1.5f + sin(2.0f * angle);
    light_desc._light_position.z = light_radius * cos(angle);
    model_transform = glm::translate(glm::mat4(1.0f), glm::vec3(light_desc._light_position));
    renderer.UpdateModelMatrix(4, model_transform);
    renderer.UpdateLightDescriptor(light_desc);

		// Draw a frame here and present to the window.
		renderer.Present();

    last_time = current_time;
	}

	return 0;
}

Model LoadModel(const char * model_path, const char * texture_path)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

	Model model;

	tinyobj::LoadObj(&attrib, &shapes, &materials, &err, model_path);

	std::unordered_map<Vertex, uint32_t> unique_verts = {};

	for (const auto& shape : shapes)
	{
		for (const auto& index : shape.mesh.indices)
		{
			Vertex vertex = {};

			vertex.pos = { attrib.vertices[3 * index.vertex_index + 0], attrib.vertices[3 * index.vertex_index + 1], attrib.vertices[3 * index.vertex_index + 2] };
			vertex.tex_coord = { attrib.texcoords[2 * index.texcoord_index + 0], 1.0f - attrib.texcoords[2 * index.texcoord_index + 1] };
			vertex.normal = { attrib.normals[3 * index.normal_index + 0], attrib.normals[3 * index.normal_index + 1], attrib.normals[3 * index.normal_index + 2] };

			if (unique_verts.count(vertex) == 0)
			{
				unique_verts[vertex] = static_cast<uint32_t>(model._vertices.size());
				model._vertices.push_back(vertex);
			}

			model._indices.push_back(unique_verts[vertex]);
		}
	}

	int32_t tex_width, tex_height, tex_channels;
	stbi_uc* model_texture = stbi_load(texture_path, &tex_width, &tex_height, &tex_channels, STBI_rgb_alpha);
	
	model._diffuse_map.channels = 4;
	model._diffuse_map.height = tex_height;
	model._diffuse_map.width = tex_width;
	model._diffuse_map.pixels = (uint8_t*)model_texture;

	return model;
}

Model LoadBillboardModel()
{
  // This is a simple quad, so we will do it by hand.
  Model model;
  model._vertices = { { glm::vec3(-0.1f, -0.1f, 0.0f),
                        glm::vec3(0.0f, 0.0f, -1.0f),
                        glm::vec2(0.0f, 1.0f) },
                      { glm::vec3(0.1f, -0.1f, 0.0f),
                        glm::vec3(0.0f, 0.0f, -1.0f),
                        glm::vec2(1.0f, 1.0f) },
                      { glm::vec3(0.1f, 0.1f, 0.0f),
                        glm::vec3(0.0f, 0.0f, -1.0f),
                        glm::vec2(1.0f, 0.0f) },
                      { glm::vec3(-0.1f, 0.1f, 0.0f),
                        glm::vec3(0.0f, 0.0f, -1.0f),
                        glm::vec2(0.0f, 0.0f) } };
  model._indices = { 0, 1, 2, 
                     2, 3, 0 };

  int32_t tex_width, tex_height, tex_channels;
  stbi_uc* model_texture = stbi_load("./textures/LightTexture.png", &tex_width, &tex_height, &tex_channels, STBI_rgb_alpha);

  model._diffuse_map.channels = 4;
  model._diffuse_map.height = tex_height;
  model._diffuse_map.width = tex_width;
  model._diffuse_map.pixels = (uint8_t*)model_texture;

  return model;
}

bool LoadShaderFromFile(const char* shader_file, std::vector<char> &shader)
{
	std::ifstream file(shader_file, std::ios::ate | std::ios::binary);
	if (!file.is_open())
	{
		return false;
	}
	auto file_size = file.tellg();

	shader.resize(file_size);
	
	file.seekg(0);
	file.read(shader.data(), file_size);
	file.close();

	return true;
}