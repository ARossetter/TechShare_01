#include "Win32App.h"
#include <windowsx.h>
#include <atlstr.h>

Win32App::Win32App()
{
}

Win32App::~Win32App()
{
}

void Win32App::CreateAppWindow(const char* class_name, const char* window_title, int width, int height)
{
	USES_CONVERSION;
	_hinst = GetModuleHandle(NULL);

	// Create and register the Windows application class.
	WNDCLASSEX wnd_class;
	wnd_class.cbSize = sizeof(WNDCLASSEX);
	wnd_class.style = CS_HREDRAW | CS_VREDRAW;
	wnd_class.lpfnWndProc = WndProc;
	wnd_class.cbClsExtra = 0;
	wnd_class.cbWndExtra = 0;
	wnd_class.hInstance = _hinst;
	wnd_class.hIcon = LoadIcon(GetModuleHandle(NULL), IDI_APPLICATION);
	wnd_class.hCursor = LoadCursor(NULL, IDC_ARROW);
	wnd_class.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wnd_class.lpszMenuName = NULL;
	wnd_class.lpszClassName = A2T(class_name);
	wnd_class.hIconSm = LoadIcon(GetModuleHandle(NULL), IDI_APPLICATION);

	RegisterClassEx(&wnd_class);

	// Now create the window.
	_hwnd = CreateWindow(A2T(class_name), A2T(window_title), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, NULL, NULL, _hinst, this);

	// Show the window.
	ShowWindow(_hwnd, SW_SHOWDEFAULT);
	SetWindowText(_hwnd, A2T(window_title));
	UpdateWindow(_hwnd);
}

bool Win32App::ProcessUpdate()
{
	bool ret = true;
	MSG msg;
	// Check for messages.
	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		if (WM_QUIT == msg.message)
		{
			ret = false;
		}
	}
	return ret;
}

LRESULT Win32App::WndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	Win32App* this_app = (Win32App*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
	if (nullptr != this_app)
	{
		return this_app->WndProc_Internal(hwnd, msg, wparam, lparam);
	}

	switch (msg)
	{
	case WM_NCCREATE:
	{
		CREATESTRUCT* create_struct = (CREATESTRUCT*)(lparam);
		SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)create_struct->lpCreateParams);
		return TRUE;
	}
	break;

	default:
		return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}

LRESULT Win32App::WndProc_Internal(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	TCHAR greeting[] = _T("Hello World!");

	switch (msg)
	{
	case WM_PAINT:
	{
		// When we don't have access to a Vulkan framebuffer, render a black screen.
		PAINTSTRUCT ps;
		HDC hdc;
		hdc = BeginPaint(hwnd, &ps);
		HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
		FillRect(hdc, &ps.rcPaint, brush);
		EndPaint(hwnd, &ps);
	}
	break;
	case WM_KEYUP:
	{
		if (_keyup_cb)
		{
			_keyup_cb(static_cast<uint32_t>(wparam));
		}
	}
	break;
	case WM_MOUSEMOVE:
	{
		if (_mousemove_cb)
		{
			_mousemove_cb(static_cast<uint16_t>(LOWORD(lparam)), static_cast<uint16_t>(HIWORD(lparam)));
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		if (_left_button_down_cb)
		{
			_left_button_down_cb(GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
		}
	}
	break;
	case WM_LBUTTONUP:
	{
		if (_left_button_up_cb)
		{
			_left_button_up_cb();
		}
	}
	break;
	case WM_SIZE:
	{
		if (_resize_cb)
		{
			_resize_cb(static_cast<uint16_t>(LOWORD(lparam)), static_cast<uint16_t>(HIWORD(lparam)));
		}
	}
	break;
	case WM_MOUSEWHEEL:
	{
		if (_mouse_wheel_cb)
		{
			auto wheel_distance = GET_WHEEL_DELTA_WPARAM(wparam);
			auto wheel_delta = WHEEL_DELTA;

			_mouse_wheel_cb(wheel_distance, wheel_delta);
		}
	}
	break;
	case WM_DESTROY:
	{
		PostQuitMessage(0);
	}
	break;
	default:
		return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}
