# Tech Share 01: Simple Lighting

This project was created as a visual aid to the tech share class presented in March 2018. It's a simple application created using Vulkan which is designed to allow for runtime-reloading of GLSL shaders.

The accompanying presentation is located here:
https://docs.google.com/presentation/d/1-q1ecZ9A7oJgeegNORWBhBBrbe8r7qdQSRnZo6vl02Y/edit?usp=sharing

![Application Output](img/display.gif)

## Building the Application

The project includes a MSVC 2017 solution. Almost all dependencies are included in the project. The one external dependency required is the Vulkan SDK, hosted by LunarG at their website: https://www.lunarg.com/vulkan-sdk/

When installing the Vulkan SDK, the installer will create the environment variables VK_SDK_PATH and VULKAN_SDK, which the Visual Studio solution uses to reference the needed headers and libraries for Vulkan.

## Controls and Key binds:

Holding down the left mouse button will rotate the camera around the origin. Rolling the mouse wheel will move the camera closer to or further from the objects in the scene.

### Keyboard Controls

*Enter* - Recompile shaders. _Note_: There is no validation or error checking of the shader at runtime. If it fails to compile, the application WILL crash.

*1-9* - Modifies the light color:
- 1: White
- 2: Red
- 3: Green
- 4: Blue
- 5: Yellow
- 6: Magenta
- 7: Cyan
- 8: 50% White
- 9: 25% White

*Minus/Plus* - Move the light source closer or further away from the models

## Contact

If you have any feedback regarding this demo, please feel free to contact the author at arossetter at gmail dot com.